FROM node:13.8.0-alpine3.10 as builder

WORKDIR /usr/src/app
COPY . /usr/src/app/

RUN npm install

FROM node:13.8.0-alpine3.10

WORKDIR /usr/src/app

COPY --from=builder /usr/src/app /usr/src/app

EXPOSE 5000
CMD ["node", "server.js"]

